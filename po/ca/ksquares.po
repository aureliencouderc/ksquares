# Translation of ksquares.po to Catalan
# Copyright (C) 2007-2021 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Albert Astals Cid <aacid@kde.org>, 2007, 2008.
# Josep M. Ferrer <txemaq@gmail.com>, 2009, 2010, 2013, 2017, 2021.
# Manuel Tortosa <manutortosa@gmail.com>, 2009.
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2015, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: ksquares\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-09-26 00:42+0000\n"
"PO-Revision-Date: 2021-10-10 14:43+0200\n"
"Last-Translator: Josep M. Ferrer <txemaq@gmail.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.12.0\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Albert Astals Cid,Manuel Tortosa Moreno"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "aacid@kde.org,manutortosa@gmail.com"

#. i18n: ectx: label, entry (NumOfPlayers), group (Game Settings)
#: ksquares.kcfg:10
#, kde-format
msgid "Number of Players"
msgstr "Nombre de Jugadors"

#. i18n: ectx: label, entry (PlayerNames), group (Game Settings)
#: ksquares.kcfg:14
#, kde-format
msgid "Player Names"
msgstr "Noms dels jugadors"

#. i18n: ectx: label, entry (HumanList), group (Game Settings)
#: ksquares.kcfg:17
#, kde-format
msgid "Human or AI"
msgstr "Humà o IA"

#. i18n: ectx: label, entry (BoardWidth), group (Game Settings)
#: ksquares.kcfg:21
#, kde-format
msgid "Width of board"
msgstr "Amplada del tauler"

#. i18n: ectx: label, entry (BoardHeight), group (Game Settings)
#: ksquares.kcfg:25
#, kde-format
msgid "Height of board"
msgstr "Alçada del tauler"

#. i18n: ectx: label, entry (QuickStart), group (Game Settings)
#: ksquares.kcfg:29
#, kde-format
msgid "Quick start the game"
msgstr "Inicia la partida de forma ràpida"

#. i18n: ectx: label, entry (Difficulty), group (AI)
#: ksquares.kcfg:35
#, kde-format
msgid "Difficulty"
msgstr "Dificultat"

#. i18n: ectx: whatsthis, entry (Difficulty), group (AI)
#: ksquares.kcfg:36
#, kde-format
msgid "How difficult the AI should be"
msgstr "Com ha de ser de difícil la IA"

#. i18n: ectx: label, entry (LineColor), group (Display)
#: ksquares.kcfg:42
#, kde-format
msgid "Line Color"
msgstr "Color de la línia"

#. i18n: ectx: label, entry (IndicatorLineColor), group (Display)
#: ksquares.kcfg:47
#, kde-format
msgid "Indicator Line Color"
msgstr "Color de la línia indicadora"

#. i18n: ectx: label, entry (HighlightColor), group (Display)
#: ksquares.kcfg:52
#, kde-format
msgid "Highlight Color"
msgstr "Color de ressaltat"

#: ksquaresdemowindow.cpp:65
#, kde-format
msgid "Player %1"
msgstr "Jugador %1"

#. i18n: ectx: ToolBar (mainToolBar)
#: ksquaresui.rc:11
#, kde-format
msgid "Main Toolbar"
msgstr "Barra d'eines principal"

#: ksquareswindow.cpp:57
#, kde-format
msgid "Current Player"
msgstr "Jugador actual"

#. i18n: ectx: property (text), item, widget (KComboBox, kcfg_Difficulty)
#: ksquareswindow.cpp:68 ksquareswindow.cpp:229 ksquareswindow.cpp:235
#: ksquareswindow.cpp:240 prefs_ai.ui:54
#, kde-format
msgid "Easy"
msgstr "Fàcil"

#. i18n: ectx: property (text), item, widget (KComboBox, kcfg_Difficulty)
#: ksquareswindow.cpp:69 ksquareswindow.cpp:230 ksquareswindow.cpp:234
#: ksquareswindow.cpp:241 prefs_ai.ui:59
#, kde-format
msgid "Medium"
msgstr "Mitjana"

#. i18n: ectx: property (text), item, widget (KComboBox, kcfg_Difficulty)
#: ksquareswindow.cpp:70 ksquareswindow.cpp:231 ksquareswindow.cpp:236
#: ksquareswindow.cpp:239 prefs_ai.ui:64
#, kde-format
msgid "Hard"
msgstr "Difícil"

#: ksquareswindow.cpp:201
#, kde-format
msgctxt "@title:window"
msgid "Scores"
msgstr "Puntuacions"

#: ksquareswindow.cpp:206
#, kde-format
msgid "Player Name"
msgstr "Nom del jugador"

#: ksquareswindow.cpp:207
#, kde-format
msgid "Completed Squares"
msgstr "Quadres completats"

#: ksquareswindow.cpp:291
#, kde-format
msgid "Start a new game with the current settings"
msgstr "Inicia una partida nova amb la configuració actual"

#: ksquareswindow.cpp:309
#, kde-format
msgid "Display"
msgstr "Pantalla"

#: ksquareswindow.cpp:314
#, kde-format
msgid "Computer Player"
msgstr "Jugador de l'ordinador"

#: main.cpp:39
#, kde-format
msgid "KSquares"
msgstr "KSquares"

#: main.cpp:41
#, kde-format
msgid ""
"Take it in turns to draw lines.\n"
"If you complete a squares, you get another go."
msgstr ""
"Fes torns per a dibuixar les línies.\n"
"Si completes un quadre, torna a jugar."

#: main.cpp:43
#, kde-format
msgid "(C) 2006-2007 Matt Williams"
msgstr "(C) 2006-2007 Matt Williams"

#: main.cpp:46
#, kde-format
msgid "Matt Williams"
msgstr "Matt Williams"

#: main.cpp:46
#, kde-format
msgid "Original creator and maintainer"
msgstr "Creador original i mantenidor"

#: main.cpp:47
#, kde-format
msgid "Fela Winkelmolen"
msgstr "Fela Winkelmolen"

#: main.cpp:47
#, kde-format
msgid "Many patches and bugfixes"
msgstr "Bastants pedaços i correccions d'errors"

#: main.cpp:48
#, kde-format
msgid "Tom Vincent Peters"
msgstr "Tom Vincent Peters"

#: main.cpp:48
#, kde-format
msgid "Hard AI"
msgstr "IA difícil"

#: main.cpp:53
#, kde-format
msgid "Run game in demo (autoplay) mode"
msgstr "Executa el joc en el mode demo (partida automàtica)"

#: main.cpp:67 main.cpp:68 main.cpp:69
#, kde-format
msgctxt "default name of player"
msgid "Player %1"
msgstr "Jugador %1"

#: newgamedialog.cpp:28
#, kde-format
msgctxt "@title:window"
msgid "New Game"
msgstr "Partida nova"

#. i18n: ectx: property (windowTitle), widget (QWidget, NewGameWidget)
#: newgamewidget.ui:13
#, kde-format
msgid "New Game"
msgstr "Partida nova"

#. i18n: ectx: property (whatsThis), widget (QGroupBox, groupBox)
#: newgamewidget.ui:37
#, kde-format
msgid "Settings for the players in the game"
msgstr "Opcions de configuració per als jugadors a la partida"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: newgamewidget.ui:40
#, kde-format
msgid "Players"
msgstr "Jugadors"

#. i18n: ectx: property (whatsThis), widget (QLabel, labelPlayer4Name)
#: newgamewidget.ui:64
#, kde-format
msgid "The name of the fourth player"
msgstr "El nom del quart jugador"

#. i18n: ectx: property (text), widget (QLabel, labelPlayer4Name)
#: newgamewidget.ui:67
#, kde-format
msgid "Player 4:"
msgstr "Jugador 4:"

#. i18n: ectx: property (whatsThis), widget (QCheckBox, playerFourHuman)
#. i18n: ectx: property (whatsThis), widget (QCheckBox, playerThreeHuman)
#. i18n: ectx: property (whatsThis), widget (QCheckBox, playerOneHuman)
#. i18n: ectx: property (whatsThis), widget (QCheckBox, playerTwoHuman)
#: newgamewidget.ui:80 newgamewidget.ui:96 newgamewidget.ui:166
#: newgamewidget.ui:214
#, kde-format
msgid "Should this player be controlled by a human"
msgstr "Si aquest jugador estarà controlat o no per un humà"

#. i18n: ectx: property (text), widget (QCheckBox, playerFourHuman)
#: newgamewidget.ui:83
#, kde-format
msgid "Hum&an?"
msgstr "H&umà?"

#. i18n: ectx: property (text), widget (QCheckBox, playerThreeHuman)
#: newgamewidget.ui:99
#, kde-format
msgid "Hu&man?"
msgstr "Hu&mà?"

#. i18n: ectx: property (whatsThis), widget (QLabel, labelPlayer3Name)
#: newgamewidget.ui:109
#, kde-format
msgid "The name of the third player"
msgstr "El nom del tercer jugador"

#. i18n: ectx: property (text), widget (QLabel, labelPlayer3Name)
#: newgamewidget.ui:112
#, kde-format
msgid "Player 3:"
msgstr "Jugador 3:"

#. i18n: ectx: property (whatsThis), widget (QLabel, labelPlayer2Name)
#: newgamewidget.ui:131
#, kde-format
msgid "The name of the second player"
msgstr "El nom del segon jugador"

#. i18n: ectx: property (text), widget (QLabel, labelPlayer2Name)
#: newgamewidget.ui:134
#, kde-format
msgid "Player 2:"
msgstr "Jugador 2:"

#. i18n: ectx: property (text), widget (QCheckBox, playerOneHuman)
#: newgamewidget.ui:169
#, kde-format
msgid "&Human?"
msgstr "&Humà?"

#. i18n: ectx: property (whatsThis), widget (QLabel, numOfPlayersLabel)
#: newgamewidget.ui:182
#, kde-format
msgid ""
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css"
"\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-"
"weight:400; font-style:normal; text-decoration:none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\">How many players will be "
"in the game</p></body></html>"
msgstr ""
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css"
"\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Sans Serif'; font-size:9pt; font-"
"weight:400; font-style:normal; text-decoration:none;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-"
"right:0px; -qt-block-indent:0; text-indent:0px;\">Quants jugadors hi haurà a "
"la partida</p></body></html>"

#. i18n: ectx: property (text), widget (QLabel, numOfPlayersLabel)
#: newgamewidget.ui:185
#, kde-format
msgid "Number of players:"
msgstr "Nombre de jugadors:"

#. i18n: ectx: property (whatsThis), widget (QLabel, labelPlayer1Name)
#: newgamewidget.ui:198
#, kde-format
msgid "The name of the first player"
msgstr "El nom del primer jugador"

#. i18n: ectx: property (text), widget (QLabel, labelPlayer1Name)
#: newgamewidget.ui:201
#, kde-format
msgid "Player 1:"
msgstr "Jugador 1:"

#. i18n: ectx: property (text), widget (QCheckBox, playerTwoHuman)
#: newgamewidget.ui:217
#, kde-format
msgid "H&uman?"
msgstr "H&umà?"

#. i18n: ectx: property (whatsThis), widget (QGroupBox, groupBox_2)
#: newgamewidget.ui:230
#, kde-format
msgid "Settings for the game board"
msgstr "Opcions de configuració per al tauler de joc"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_2)
#: newgamewidget.ui:233
#, kde-format
msgid "Game Board"
msgstr "Tauler de joc"

#. i18n: ectx: property (whatsThis), widget (QLabel, labelWidth)
#: newgamewidget.ui:257
#, kde-format
msgid "The number of squares the area is wide"
msgstr "El nombre de quadres d'amplada"

#. i18n: ectx: property (text), widget (QLabel, labelWidth)
#: newgamewidget.ui:260
#, kde-format
msgid "Width:"
msgstr "Amplada:"

#. i18n: ectx: property (whatsThis), widget (QLabel, labelHeight)
#: newgamewidget.ui:289
#, kde-format
msgid "The number of squares the area is high"
msgstr "El nombre de quadres d'alçada"

#. i18n: ectx: property (text), widget (QLabel, labelHeight)
#: newgamewidget.ui:292
#, kde-format
msgid "Height:"
msgstr "Alçada:"

#. i18n: ectx: property (whatsThis), widget (QGroupBox, groupBox_3)
#: newgamewidget.ui:324
#, kde-format
msgid "Settings for the game"
msgstr "Opcions de configuració per a la partida"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_3)
#: newgamewidget.ui:327
#, kde-format
msgid "Game Settings"
msgstr "Opcions de configuració del joc"

#. i18n: ectx: property (whatsThis), widget (QCheckBox, quickStartCheck)
#: newgamewidget.ui:351
#, kde-format
msgid "Partially fill in the board automatically before the game starts"
msgstr "Omple parcialment el tauler abans de començar la partida"

#. i18n: ectx: property (text), widget (QCheckBox, quickStartCheck)
#: newgamewidget.ui:354
#, kde-format
msgid "Quick start"
msgstr "Inici ràpid"

#. i18n: ectx: property (windowTitle), widget (QWidget, prefs_ai)
#: prefs_ai.ui:13
#, kde-format
msgid "AI Settings"
msgstr "Paràmetres de la IA"

#. i18n: ectx: property (whatsThis), widget (QLabel, label_difficulty)
#: prefs_ai.ui:75
#, kde-format
msgid "How difficult it will be to beat the computer"
msgstr "Com serà de difícil guanyar a l'ordinador"

#. i18n: ectx: property (text), widget (QLabel, label_difficulty)
#: prefs_ai.ui:78
#, kde-format
msgid "AI difficulty:"
msgstr "Dificultat de la IA:"

#. i18n: ectx: property (windowTitle), widget (QWidget, prefs_display)
#: prefs_display.ui:13
#, kde-format
msgid "Display Settings"
msgstr "Configuració de la pantalla"

#. i18n: ectx: property (whatsThis), widget (QGroupBox, groupBox_2)
#: prefs_display.ui:19
#, kde-format
msgid "Settings for colors"
msgstr "Configuració per als colors"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox_2)
#: prefs_display.ui:22
#, kde-format
msgid "Colors"
msgstr "Colors"

#. i18n: ectx: property (whatsThis), widget (QLabel, label)
#: prefs_display.ui:28
#, kde-format
msgid "Color of the drawn lines"
msgstr "Color de les línies dibuixades"

#. i18n: ectx: property (text), widget (QLabel, label)
#: prefs_display.ui:31
#, kde-format
msgid "Standard line color:"
msgstr "Color de les línies estàndard:"

#. i18n: ectx: property (whatsThis), widget (QLabel, label_2)
#. i18n: ectx: property (whatsThis), widget (QLabel, label_3)
#: prefs_display.ui:44 prefs_display.ui:60
#, kde-format
msgid "Color of the indicator lines"
msgstr "Color de les línies indicadores"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: prefs_display.ui:47
#, kde-format
msgid "Indicator line color:"
msgstr "Color de la línia indicadora:"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: prefs_display.ui:63
#, kde-format
msgid "Highlight color:"
msgstr "Color de ressaltat:"

#. i18n: ectx: property (windowTitle), widget (QWidget, ScoresWidget)
#: scoreswidget.ui:13
#, kde-format
msgid "Scores"
msgstr "Puntuacions"

#. i18n: ectx: property (whatsThis), widget (QTableView, scoreTable)
#: scoreswidget.ui:19
#, kde-format
msgid "Scoreboard"
msgstr "Taula de puntuacions"

#~ msgid "Online rankings"
#~ msgstr "Classificació en línia"

#~ msgid "List of seats"
#~ msgstr "Llista de llocs"

#~ msgid "Request to start a new game with the current settings"
#~ msgstr "Sol·licita iniciar una nova partida amb l'arranjament actual"

#~ msgid "The game has finished"
#~ msgstr "La partida ha acabat"

#~ msgid "It is your turn"
#~ msgstr "És el vostre torn"

#~ msgid "Waiting for opponent..."
#~ msgstr "S'està esperant un contrincant..."

#~ msgid "Waiting for move result..."
#~ msgstr "S'està esperant el resultat de la jugada..."

#~ msgid "Online game"
#~ msgstr "Joc en línia"
